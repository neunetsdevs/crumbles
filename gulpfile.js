var gulp = require('gulp'),
    minifycss = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    notify = require('gulp-notify'),
    revison = require('gulp-rev');

// gulp.task('default', function() {
//     console.log('tableau Gulp Started');
// });

gulp.task('minifycss-version', function() {
    return gulp.src(['static/parser/*.css'])
        .pipe(concat('tableau.css'))
        .pipe(minifycss().on('error', function(e) { console.log(e); }))
        .pipe(gulp.dest('static/build/css/'))
        .pipe(notify({ message: 'style minified' }))
        .pipe(gulp.src('static/build/css/tableau.css', { base: 'static' }))
        .pipe(gulp.dest('static'))
        .pipe(revison())
        .pipe(gulp.dest('static'))
        .pipe(revison.manifest())
        .pipe(gulp.dest('static/build'))
        .pipe(notify({ message: 'Versioned CSS' }));
});


// gulp.task('version', function() {
//     return gulp.src('static/build/css/tableau.css', { base: 'static' })
//         .pipe(gulp.dest('static'))
//         .pipe(revison())
//         .pipe(gulp.dest('static'))
//         .pipe(revison.manifest())
//         .pipe(gulp.dest('static/build'));
// });

// gulp.task('default', ['minifycss', 'version']);
gulp.task('default', ['minifycss-version']);
