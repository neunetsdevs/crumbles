# * Created by Neunets pvt. Ltd., for crumbles 
# * User: Arasu.B
# * Date: 1/4/2017
# * Time: 6:12 PM
#

from django.conf.urls import url
from . import views
import django.views.defaults

app_name = 'parser'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^parser/', views.parser, name='parser'),
    # url(r'^file_upload/', views.file_upload, name='file_upload'),
    # url(r'^404/$', django.views.defaults.page_not_found, ),
    # url(r'^404/$', views.error404, name='error404'),
]
