from django.contrib import admin
from .models import crumblesInfo
from django.contrib.admin import SimpleListFilter
import datetime
# Register your models here.
#
# class crumbstimestampFilter(SimpleListFilter):
#     title = 'CrumbsDate'
#     parameter_name = 'crumbstimestamp'
#
#     def lookups(self, request, model_admin):
#         CrumbsDate = []
#         for c in model_admin.model.objects.all():
#             if c.crumbstimestamp != None:
#                 # CrumbsDate.append(datetime.datetime.strptime(c.crumbstimestamp, "%Y-%m-%d"))
#                 CrumbsDate.append(c.crumbstimestamp.split(" ")[0])
#             else:
#                 CrumbsDate.append(u"2017-02-15")
#                 # CrumbsDate.append(c.crumbstimestamp)
#
#         # CrumbsDate = list([() for c in model_admin.model.objects.all() ])
#         # return [CrumbsDate]
#         return [set(c for c in CrumbsDate)]
#
#     def queryset(self, request, queryset):
#         if self.value():
#             return queryset.filter(CrumbsDate=self.value())
#         else:
#             return queryset

class crumblesInfoAdmin(admin.ModelAdmin):
    def CrumbsDate(self, obj):
        # return obj.timefield.strftime("%Y - %m %d")
        if obj.crumbstimestamp != None:
            # return datetime.datetime.strftime(datetime.datetime.strptime(obj.crumbstimestamp.split(" ")[0], "%Y-%m-%d"), "%Y-%B-%d")
            return obj.crumbstimestamp.split(" ")[0]
        else:
            return obj.crumbstimestamp
    CrumbsDate.admin_order_field = 'crumbstimestamp'
    CrumbsDate.short_description = 'crumbstimestamp'

    list_display = ('tableauworkbookname', 'emailid', 'phonenumber', 'CrumbsDate')
    search_fields = ('tableauworkbookname', 'emailid', 'phonenumber', 'crumbstimestamp')
    # list_filter = ('crumbstimestamp', 'emailid', )
    # list_filter = ('crumbstimestamp', 'emailid', crumbstimestampFilter )

admin.site.register(crumblesInfo, crumblesInfoAdmin)