# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime

class Migration(migrations.Migration):

    dependencies = [
        ('CookieCrumbles', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='crumblesInfo',
            name='crumbstimestamp',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
