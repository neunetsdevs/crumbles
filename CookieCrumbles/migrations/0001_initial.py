# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='crumblesInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('companyname', models.CharField(max_length=200, null=True, blank=True)),
                ('tableauworkbookname', models.CharField(max_length=200, null=True, blank=True)),
                ('about', models.CharField(max_length=500, null=True, blank=True)),
                ('requestedby', models.CharField(max_length=200, null=True, blank=True)),
                ('developedby', models.CharField(max_length=200, null=True, blank=True)),
                ('approvedby', models.CharField(max_length=200, null=True, blank=True)),
                ('emailid', models.CharField(max_length=200)),
                ('requesteddate', models.CharField(max_length=200, null=True, blank=True)),
                ('completeddate', models.CharField(max_length=200, null=True, blank=True)),
                ('revisionnumber', models.CharField(max_length=200, null=True, blank=True)),
                ('phonenumber', models.CharField(max_length=200, null=True, blank=True)),
            ],
        ),
    ]
