# * Created by Neunets pvt. Ltd., for crumbles 
# * User: Arasu.B
# * Date: 1/27/2017
# * Time: 8:14 PM
#

from django.db import models
import datetime

class crumblesInfo(models.Model):
    companyname = models.CharField(max_length=200, null=True, blank=True)
    tableauworkbookname = models.CharField(max_length=200, null=True, blank=True)
    about = models.CharField(max_length=500, null=True, blank=True)
    requestedby = models.CharField(max_length=200, null=True, blank=True)
    developedby = models.CharField(max_length=200, null=True, blank=True)
    approvedby = models.CharField(max_length=200, null=True, blank=True)
    emailid = models.CharField(max_length=200)
    requesteddate = models.CharField(max_length=200, null=True, blank=True)
    # requesteddate = models.DateTimeField('requesteddate' , null=True, blank=True,default=datetime.date.today)
    completeddate = models.CharField(max_length=200, null=True, blank=True)
    revisionnumber = models.CharField(max_length=200, null=True, blank=True)
    phonenumber = models.CharField(max_length=200, null=True, blank=True)
    crumbstimestamp = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return u'%s %s %s %s' % (self.tableauworkbookname, self.emailid, self.phonenumber, self.crumbstimestamp)

class UsersInfo(models.Model):
    """
        Model for users info store
    """
    name = models.CharField(max_length=200, null=True, blank=True)
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    phonenumber = models.CharField(max_length=200, null=True, blank=True)
    companyname = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        """
            self function for filter on admin
        """
        return u'%s %s %s %s %s' % (self.username, self.password, self.name, self.phonenumber, self.companyname)
    