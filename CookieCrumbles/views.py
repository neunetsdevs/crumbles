"""
# @Author: Arasu
# @Date:   2017-04-01T11:54:22+05:30
# @Email:  arasu@neunets.com
# @Project: CRUMBS
# @Filename: views.py
# @Last modified by:   Arasu
# @Last modified time: 2017-04-27T09:04:48+05:30
# @Copyright: Neunets Pvt. Ltd.,
"""

import datetime
import json
import os
import time
import traceback
import zipfile
from shutil import copyfile
from urlparse import urlparse
from xml.etree import ElementTree as ET

import pdfkit
from django.core.files.storage import FileSystemStorage
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from json2html import json2html, sys
from markdown import markdown
from prettytable import PrettyTable
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import csrf_exempt
from CookieCrumbles.forms import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout, login
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.views.decorators import csrf


from . import models

# from tabulate import tabulate


# from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def index():
    """index"""
    return HttpResponse("Welcome to Tableau crumbs")

#END User License Agrement
def eula(request):
    """eula"""
    # return render(request, 'parser/crumbles.html', {'EULA': "EULA"})
    return render(request, 'parser/eula.html', {'EULA': "EULA"})

def error404(request):
    """404 error."""
    # return render(request, '404.html')
    response = render_to_response('404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response

def handler404(request):
    """404 error Handler."""
    response = render_to_response('404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response
@csrf_exempt
def login_page(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/accounts/home')
    else:
        if request.method == "POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                user = authenticate(username=form.cleaned_data["username"],
                                    password=form.cleaned_data["password"])
                login(request, user)
                return HttpResponseRedirect('/accounts/home')
            else:
                error = True
                error_msg = 'Username and password did not match'
                return render_to_response('registration/login.html',
                                          {'error':error,
                                           'error_msg':error_msg})

    return render_to_response('registration/login.html')

@csrf_protect
def register(request):
    """Register User."""
    if request.user.is_authenticated():
        return HttpResponseRedirect('/accounts/home')
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password1'],
                email=form.cleaned_data['email'])
            return render_to_response('registration/success.html')
    else:
        form = RegistrationForm()
    variables = RequestContext(request, {
    'form': form
    })
 
    return render_to_response(
    'registration/register.html',
    variables,
    )
 
def register_success(request):
    return render_to_response('registration/success.html')
 
def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/accounts/login')

@login_required
def home(request):
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                request.session.set_expiry(86400) #sets the exp. value of the session 
                login(request, user)
    return render_to_response(
    'home.html',{'is_auth':request.user.is_authenticated()},
     context_instance=RequestContext(request))
    #{ 'user': request.user }

# @csrf_exempt
def parser(request):
    """parser"""
    # to_email = ['leads@cookieanalytix.com']
    to_email = ['arasu@cookieanalytix.com']
    from_email = 'hello@cookieanalytix.com'
    url = email_id = filename = ''
    try:
        parsed_json = {}
        media = "/media/"
        media_uploads = "/media/uploads/"
        file_system = FileSystemStorage()
        # print(request.get('scheme'))
        print request.scheme
        # print(request.META.get('SERVER_PROTOCOL'))
        # print(request.META.get('wsgi.url_scheme'))
        # print(os.environ["REQUEST_URI"])
        # urlscheme = request.META.get('wsgi.url_scheme')
        urlscheme = request.scheme
        if urlscheme == '':
            urlscheme = 'http'
        # urlscheme = 'https'
        # if post request came
        if request.method == 'POST':
            # getting values from post
            crumbstime = time.time()
            crumbstimestap = datetime.datetime.fromtimestamp(
                crumbstime).strftime('%Y-%m-%d %H:%M:%S')
            print 'Post started' + crumbstimestap

            logo = request.POST.get('logo')
            company_name = request.POST.get('company_name')
            # tableau_workbook_name = request.POST.get('tableau_workbook_name')
            about = request.POST.get('about')
            uploaded_by = request.POST.get('uploaded_by')
            developed_by = request.POST.get('developed_by', '')
            approved_by = request.POST.get('approved_by')
            email_id = request.POST.get('email_id')
            date_approved = request.POST.get('date_approved')
            phone_number = request.POST.get('phone_number')

            uploadedfile = request.FILES['uploadedfile']
            filename = file_system.save(uploadedfile.name, uploadedfile)
            filename = filename.encode("utf-8")
            uploaded_file_url = file_system.url(filename)
            url = os.getcwd() + media + filename
            tableau_workbook_name = uploadedfile
            if not os.path.exists(os.getcwd() + media_uploads):
                os.makedirs(os.getcwd() + media_uploads)
            if not os.path.exists(os.getcwd() + media_uploads + filename):
                os.makedirs(os.getcwd() + media_uploads + filename)
            if not os.path.exists(os.getcwd() + media_uploads + filename + '/logo'):
                os.makedirs(os.getcwd() + media_uploads + filename + '/logo')
            if logo != None:
                logo = request.FILES['logofile']
                file_system = FileSystemStorage(location=os.getcwd() + media_uploads \
                    + filename + '/logo')
                logofilename = file_system.save(logo.name, logo)
            else:
                logofilename = None

            print logofilename
            crumbstime = time.time()
            crumbstimestampnow = str(datetime.datetime.fromtimestamp(
                crumbstime).strftime('%Y-%m-%d %H:%M:%S'))
            crumbstimestampnow = unicode(crumbstimestampnow, "utf-8")
            print crumbstimestampnow

            insert_obj = models.crumblesInfo(companyname=company_name,
                                             tableauworkbookname=tableau_workbook_name,
                                             about=about,
                                             requestedby=uploaded_by,
                                             developedby=developed_by,
                                             approvedby=approved_by,
                                             emailid=email_id,
                                             completeddate=date_approved,
                                             phonenumber=phone_number,
                                             crumbstimestamp=crumbstimestampnow)
            insert_obj.save()

            uploadedfileinfo = {}
            fileinfo = os.stat(os.getcwd() + media + filename.decode('utf-8'))
            (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(
                os.getcwd() + media + filename.decode('utf-8'))
            print fileinfo
            print (mode, ino, dev, nlink, uid, gid)
            print "last modified: %s" % time.ctime(mtime)
            uploadedfileinfo['mtime'] = time.ctime(mtime)
            uploadedfileinfo['atime'] = time.ctime(atime)
            uploadedfileinfo['ctime'] = time.ctime(ctime)
            uploadedfileinfo['size'] = size / 1024

            if os.path.splitext(filename.decode('utf-8'))[1] == '.twbx':
                # print(url)
                with zipfile.ZipFile(url, "r") as zip_ref:
                    zip_ref.extractall(os.getcwd() + media_uploads + filename.decode('utf-8'))
                    # tableau_file = os.path.splitext(filename)[0]
                    extract_file_list = zip_ref.namelist()
                print extract_file_list[0]
                tableau_file = ''
                os.mkdir(os.getcwd() + media_uploads + filename + '/output')
                for twbfile in extract_file_list:
                    extension = os.path.splitext(twbfile)[1]
                    if extension == '.twb':
                        tableau_file = twbfile
                        print os.getcwd() + media_uploads + filename + '/' + tableau_file
                print os.getcwd() + media_uploads + filename + '/' + tableau_file
            elif os.path.splitext(filename)[1] == '.twb':
                os.mkdir(os.getcwd() + media_uploads + filename + '/output')
                print url
                # print(url.decode('utf-8'))
                url = url.replace('\\', '/')
                copyfile(url.encode('utf-8'), os.getcwd() + media_uploads + filename + '/'
                         + str(uploadedfile))
                tableau_file = str(uploadedfile)
            else:
                return render(request, 'parser/crumbles.html',
                              {'wronginput': "Only twbx or twb files"})

            js_script = '<script> $(document).ready(function() { $("tbody").addClass("header"); ' \
                 '$("tr:contains(\'dimension\')").addClass(\'blueBg\'); ' \
                 '$("tr:contains(\'measure\')").addClass(\'greenBg\');});</script >'

            d3_data = {}
            page1 = '<div class="page1 watermarkimg"> ' \
                    '<br/><br/><br/><br/>' \
                    '<div class="container crumbs_container">'\
                    '<div class="table crumbs_table_head" > ' \
                    '<div class="no_margin handbook_title_head text-center" align="center">'\
                        + str(tableau_file) +\
                    '<span class="handbook-size text-center">-Handbook</span></div></span>'\
                    '</div>' \
                    '<p class="page1-table page1-line"></p>' \
                    '<div class="crumbs_circle">'\
                    '<table class="page1-table crumbs_table table "><tbody>'\
                    '<tr><th><strong>Company Name:</strong></th>' \
                        '<td>' + str(company_name) + '</td></tr>' \
                    '<tr><th><strong>About the File:</strong></th>' \
                        '<td><span >' + str(about) + '</span></td></tr>' \
                    '<tr><th><strong>Uploaded By:</strong></th>' \
                        '<td>'+ str(uploaded_by) + '</td></tr>' \
                    '<tr><th><strong>Developed by:</strong></th>' \
                        '<td>'+ str(developed_by) + '</td></tr>' \
                    '<tr><th><strong>Approved by:</strong></th>' \
                        '<td>' + str(approved_by) + '</td></tr>' \
                    '<tr><th><strong>Date Approved :</strong></th>' \
                        '<td>' + str(date_approved) + '</td>'\
                    '</tr></tbody></table>'\
                    '</div>'\
                    '<p class="page1-table page1-line page1-line-bottom "></p>' \
                    '<p class="page_break"></p><br />' \
                    '<p class="page_break"></p><br />' \
                    '<div class="center_div"><div class="watermark">&nbsp;</div>'\
                    '</div></div></div>'

            twb_file = ET.parse(os.getcwd() + media_uploads + filename + '/' + tableau_file)
            twbroot = twb_file.getroot()
            # print twbroot.tag, twbroot.attrib
            parsed_json['workbookInformation'] = twbroot.attrib
            parsed_json['workbookInformation']['Name'] = os.path.splitext(tableau_file)[0]
            # parsed_json['workbookInformation']['Uploaded On'] = uploadedfileinfo['ctime']
            parsed_json['workbookInformation']['File Size'] = str(uploadedfileinfo['size']) + ' KB'

            d3_data["children"] = ''
            twb_datasources = list(twbroot.find('datasources'))

            # For Datasource and dimensions & Measures
            i = 1
            # columns Variable
            column_name = []
            column_role = []
            column_datatype = []
            column_type = []
            # Calculations Variable
            calculation_name = []
            calculation_role = []
            calculation_datatype = []
            calculation_type = []
            calculation_caption = []
            calculation_formula = []
            calculation_dependency = []
            calculation_name_caption = {}

            parsed_json['workbookInformation']['Datasources'] = len(twbroot.findall(
                "./datasources/datasource"))
            parsed_json['workbookInformation']['Dashboards'] = len(twbroot.findall(
                "./dashboards/dashboard"))
            parsed_json['workbookInformation']['Worksheets'] = len(twbroot.findall(
                "./worksheets/worksheet"))
            datasource_count = 0
            unused_datasource_count = 0
            datasource_count = len(twbroot.findall("./datasources/datasource"))
            unused_datasource_count = len(twbroot.findall(
                "./datasources/datasource[@hasconnection]"))
            datasource_count = datasource_count - unused_datasource_count
            print ":::::Datasource Count::::", datasource_count
            page2start = ''
            page2start = '<p class="pb_before"><br/></p>' \
                '<div class="page2 watermarkimg" id="page2"><br/><br/>' \
                '<div id="page2-heading"><h1 class="crumbs page2-h1">Workbook Summary</h1><br/>' \
                '</div><table class="page1-table crumbs_table table" bgcolor="khaki" >' \
                '<tbody><tr><th > Name </th>' \
                '<td class="table-column">' + str(parsed_json['workbookInformation']['Name']) +'' \
                '</td></tr><tr><th> File Size </th><td class="table-column">' \
                '' + str(parsed_json['workbookInformation']['File Size']) +'</td>' \
                '</tr><tr><th> Version/Source Build </th><td class="table-column">' \
                ''+ twbroot.get('version') +' / ' + str(twbroot.get('source-build')) +'</td>' \
                '</tr><tr><th> Source Platform </th>' \
                '<td class="table-column">' + str(twbroot.get('source-platform')) + '</td></tr>' \
                '<tr><th> No. Of Dashboards </th><td class="table-column">' \
                '' + str(len(twbroot.findall("./dashboards/dashboard"))) + '</td></tr>' \
                '<tr><th> No. Of Worksheets </th><td class="table-column">' \
                '' + str(len(twbroot.findall("./worksheets/worksheet"))) + ' </td> </tr>' \
                '<tr><th> No. Of Datasources </th>' \
                '<td class="table-column">' + str(datasource_count) + ' </td></tr>' \
                '<tr><th> Datasources Details</th><td class="table-column"> ' \
                '<table class="datasourcedetails-table"><thead><tr><th class="table-th">Name</th>' \
                '<th class="table-th">Caption </th><th class="table-th">Source</th>' \
                '<th class="table-th">Type</th><th class="table-th">Total No. Of Columns</th>' \
                '<th style="border-right:none;" class="table-th">Joins</th>' \
                '</tr></thead><tbody>'
            page2datasource = ''
            page4datasourcedetails = ''
            # d3_data_datasources = []
            for child in twb_datasources:
                if child.tag == "datasource":
                    attribute_list = child.attrib
                    if 'excel' in attribute_list['name']:
                        attribute_list['name'] = 'Excel File'
                    hasconnection = 'hasconnection'
                    if hasconnection in attribute_list:
                        continue
                    parsed_json['Datasources-' + str(i)] = attribute_list
                    if not twbroot.findall("./datasources/datasource/connections"):
                        parsed_json['Datasources-' + str(i)]['Source'] = 'Parameters'
                    for step_child in child:
                        if step_child.tag == "connection":
                            (parsed_json['Datasources-' + str(i)]).update(step_child.attrib)

                            if float(child.get('version')) >= float(10):
                                if child.get('caption') != None:
                                    datasource_connection_source = twbroot.findall(
                                        "./datasources/datasource/[@caption='" + child.get(
                                            'caption') + "']/connection/" \
                                            "named-connections/named-connection/connection")
                                elif child.get('name') != None:
                                    datasource_connection_source = twbroot.findall(
                                        "./datasources/datasource/[@name='" + child.get(
                                            'name') + "']/connection/" \
                                            "named-connections/named-connection/connection")
                                # print(datasource_connection_source[0])
                                if child.get('caption') or child.get('name') != None:
                                    for source in datasource_connection_source:
                                        # print source.attrib
                                        parsed_json['Datasources-' + str(i)]['Source'] = source.get(
                                            'class')
                            else:
                                parsed_json['Datasources-' + str(i)]['Source'] = step_child.get(
                                    'class')
                        if step_child.tag == "column":
                            calculation = step_child.getchildren()
                            if calculation and calculation[0].tag == "calculation":
                                # read the calculation attributes and update
                                step_child.attrib.update(calculation[0].attrib)
                                calculation_name.append(step_child.get('name'))
                                calculation_datatype.append(step_child.get('datatype'))
                                calculation_role.append(step_child.get('role'))
                                calculation_type.append(step_child.get('type'))
                                calculation_formula.append(step_child.get('formula'))
                                if step_child.get('caption') is None:
                                    calculation_caption.append(step_child.get('name'))
                                else:
                                    calculation_caption.append(step_child.get('caption'))
                                calculation_name_caption[step_child.get('name')] = step_child.get(
                                    'caption')
                                # Find the dependecy of the calculations
                                # calculations_dependency= ''
                                calculations_dependency_column = twbroot.findall(
                                    "./datasources/datasource/[@name='" + child.get(
                                        'name') + "']/column")
                                # print("./datasources/datasource/[@caption='" + child.get(
                                # 'name') + "']/column/calculation")
                                dependency = ''
                                for calculations_parent in calculations_dependency_column:
                                    calculation_column = calculations_parent.getchildren()
                                    if calculation_column and calculation_column[
                                            0].tag == "calculation":
                                        # print  " --- Formula : ", calculation_column[0].get(
                                        #     'formula'), " --- NAME : ", step_child.get('name')
                                        if calculation_column[0].get('formula') != None and str(
                                                step_child.get('name')) in calculation_column[
                                                    0].get('formula'):
                                            # dependency= dependency+
                                            # '<li class="calculation-dependency">'
                                            # + str(calculations_parent.get('name')) + '</li>'
                                            # dependency= dependency+
                                            # '<td class="calculation-dependency">'
                                            # + str(calculations_parent.get('name')) + '</td>'
                                            dependency = dependency + str(
                                                calculations_parent.get('name')) + ' ,  '
                                calculation_dependency.append(dependency)
                            else:
                                column_name.append(step_child.get('name'))
                                column_role.append(step_child.get('role'))
                                column_datatype.append(step_child.get('datatype'))
                                column_type.append(step_child.get('type'))

                    #pretty Table Code Starts
                    datasource_columns = {}
                    datasource_columns['Name'] = {}
                    datasource_columns['Role'] = {}
                    datasource_columns['Data Type'] = {}
                    datasource_columns['Type'] = {}

                    datasource_columns['Name'] = column_name
                    datasource_columns['Role'] = column_role
                    datasource_columns['Data Type'] = column_datatype
                    datasource_columns['Type'] = column_type

                    columns = PrettyTable()
                    sort_order = ['Name', 'Data Type', 'Type', 'Role']
                    for k, val in sorted(datasource_columns.iteritems(), key=lambda (
                            k, v): sort_order.index(k)):
                        # columns.add_column(key, sorted(val))
                        columns.add_column(k, val)

                    columns = columns.get_html_string(attributes={"size": "100%", "class": "tbl-width page1-table crumbs_table table datasource-Columns"}, sortby="Role")
                    if child.get('name') == 'Parameters':
                        parsed_json['Datasources-' + str(i)]['Total No. Of Columns'] = len(
                            twbroot.findall(
                                "./datasources/datasource/[@name='" + child.get(
                                    'name') + "']/column"))
                    else:
                        parsed_json['Datasources-' + str(i)]['Total No. Of Columns'] = len(
                            twbroot.findall(
                                "./datasources/datasource/[@name='" + child.get('name')
                                + "']/connection/metadata-records/" \
                                "metadata-record[@class='column']"))
                    if parsed_json['Datasources-' + str(i)]['Total No. Of Columns'] == 0:
                        parsed_json['Datasources-' + str(i)]['Total No. Of Columns'] = (len(
                            twbroot.findall(
                                "./datasources/datasource/[@name='" + child.get(
                                    'name') + "']/column"))) - (len(twbroot.findall(
                                        "./datasources/datasource/[@name='" + child.get(
                                            'name') + "']/column/calculation")))

                    parsed_json['Datasources-' + str(i)]['Used Columns Count'] = len(column_name)
                    parsed_json['Datasources-' + str(i)]['Calculation Count'] = len(
                        calculation_name)
                    if twbroot.findall("./datasources/datasource/[@name='" + child.get(
                            'name') + "']/extract"):
                        datasource_type = 'Extract'
                    else:
                        datasource_type = 'Live'
                    parsed_json['Datasources-' + str(i)]['Type'] = datasource_type

                    #dafault Datasource relations
                    print './datasources/datasource/[@caption="' + str(
                        child.get('caption')) + '"]/connection/relation'
                    datasource_joins = twbroot.findall(
                        './datasources/datasource/[@caption="' + str(
                            child.get('caption')) + '"]/connection/relation')
                    join = None
                    for joins in datasource_joins:
                        # print Joins.attrib
                        if(joins.get('type')) == 'join':
                            join = 'Yes'

                    page2datasource = page2datasource + '<tr><td class="table-column">' + str(
                        child.get('name')) + '</td><td class="table-column">' + str(
                            child.get('caption')) + '</td><td class="table-column">' \
                                       + str(parsed_json['Datasources-' + str(i)][
                                           'Source']) + '</td><td class="table-column">' + str(
                                               datasource_type) + '</td>' + \
                                      '<td class="table-column">' + str(
                                          parsed_json['Datasources-' + str(i)][
                                              'Total No. Of Columns']) + \
                                       '</td><td class="table-column"' \
                                        'style="border-right:none;">' + str(join) + '</td></tr>'

                    # Columns Reset
                    column_name = []
                    column_role = []
                    column_datatype = []
                    column_type = []
                    print len(calculation_name_caption)

                    #pretty Table Code Starts
                    datasource_calculation = {}
                    datasource_calculation['Caption'] = {}
                    datasource_calculation['Role'] = {}
                    datasource_calculation['Data Type'] = {}
                    datasource_calculation['Type'] = {}
                    datasource_calculation['Formula'] = {}
                    datasource_calculation['Dependency'] = {}

                    datasource_calculation['Caption'] = calculation_caption
                    datasource_calculation['Role'] = calculation_role
                    datasource_calculation['Data Type'] = calculation_datatype
                    datasource_calculation['Type'] = calculation_type
                    datasource_calculation['Formula'] = calculation_formula
                    datasource_calculation['Dependency'] = calculation_dependency

                    calculations = PrettyTable()

                    for key, val in sorted(datasource_calculation.iteritems()):
                        calculations.add_column(key, val)

                    calculations = calculations.get_html_string(attributes={"size": "100%", "class": "page1-table crumbs_table pg-break table tbl-width datasource-Calculations"}, sortby="Role")
                    # Replace the calcualtion names Starts
                    calculation_name_caption_count = 0
                    print len(calculation_name_caption)
                    while calculation_name_caption_count < len(calculation_name_caption):
                        if calculation_caption[calculation_name_caption_count] != None:
                            calculations = calculations.replace(
                                calculation_name[calculation_name_caption_count],
                                calculation_caption[calculation_name_caption_count])
                        # print(calculation_name_caption)
                        calculation_name_caption_count += 1
                    # Replace the calcualtion names ENDs

                    # Calculation Reset
                    calculation_name = []
                    calculation_role = []
                    calculation_datatype = []
                    calculation_type = []
                    calculation_caption = []
                    calculation_formula = []
                    calculation_dependency = []
                    calculation_name_caption = {}

                    # print(parsed_json['Datasources-' + str(i)])

                    # Remove unwanted elements
                    if 'password' in parsed_json['Datasources-' + str(i)]:
                        del parsed_json['Datasources-' + str(i)]['password']

                    # Datasource HTML
                    datasource_json = json.dumps(parsed_json['Datasources-' + str(i)])
                    datasource_json = dict((k.title(), v) for k, v in parsed_json[
                        'Datasources-' + str(i)].iteritems())

                    for k in datasource_json.keys():
                        if datasource_json[k] == "":
                            del datasource_json[k]

                    datasourcehtml = json2html.convert(json=datasource_json,
                                                       table_attributes="id=\"datasources-table\"" \
                                                       "class=\"page1-table crumbs_table table" \
                                                       "tbl-width Dashboard-" + str(i) + '"')

                    # datasource_name = child.get('name')

                    if child.get('name') == 'Parameters':
                        datasource_name = 'Parameters'
                    else:
                        # print(str(child.get('caption')))
                        if str(child.get('caption')) == 'None':
                            datasource_name = str(child.get('name'))
                        else:
                            datasource_name = str(child.get('caption'))


                    page4datasourcedetails = page4datasourcedetails + \
                        '<div class="datasource Datasource-' + str(i) \
                        + '"><span><h2>' + datasource_name  \
                        + '</h2></span>'+ datasourcehtml + \
                        '<div class="datasource-column datasource">' \
                        '<h3> Column Details </h3> ' + columns + '</div>' \
                        + '<div class="datasource-calculations datasource">' \
                        '<h3> Calculation Details </h3> ' + calculations + \
                        '</div></div> <hr />'
                    # Iterate I For Data sources
                    i = i + 1
            datasource_cols = []
            worksheet_cols = []

            #Get the elements from datasource and worksheets
            datasource_cols = list(twbroot.findall("./datasources/datasource/column"))
            worksheet_cols = list(twbroot.findall(
                './worksheets/worksheet/table/view/datasource-dependencies/column'))

            # List and variables
            data_list = []
            work_list = []
            column_ds_name = []
            column_ws_name = []
            datasource_name = ""
            worksheet_name = ""

            # To get the column names from datasource
            for datasource_col in datasource_cols:
                data_list = datasource_col.attrib
                datasource_name = data_list['name']
                column_ds_name.append(datasource_name)
            #print ":::::column_ds_name::::", column_ds_name

            # To get the column names from worksheets
            for worksheet_col in worksheet_cols:
                work_list = worksheet_col.attrib
                worksheet_name = work_list['name']
                column_ws_name.append(worksheet_name)
            #print ":::column_ws_name:::::", column_ws_name

            # Finding out the list of column names which is not used in any worksheets
            unused_col_name = [item for item in column_ds_name if item not in column_ws_name]
            #print "::::::unused_col_name:::::", unused_col_name

            # List the name,datatype,role and type of unused column and eliminate the dublicates
            unused_html = ""
            column_names = []
            for datasource_col in datasource_cols:
                unused_column = datasource_col.attrib
                if unused_column['name'] in unused_col_name:
                    if unused_column['name'] not in column_names:
                        unused_html = unused_html + "<tr><td>"+ \
                        unused_column['name'] +"</td><td>"+ \
                        unused_column['datatype'] +"</td><td>"+ \
                        unused_column['role'] +"</td><td>"+ \
                        unused_column['type'] +"</td></tr>"
                    column_names.append(unused_column['name'])

            # d3_data['children'] =  d3_data_datasources
            xml_datasource_list = json.dumps(parsed_json)
            xml_datasource_list = json2html.convert(json=xml_datasource_list,
                                                    table_attributes="id=\"datasource-table\"" \
                                                    "class=\"datasource\"")

            page2 = page2start + page2datasource + '</tbody></table></td> </tr></tbody></table>'
            # List of Worksheets
            worksheet_list = []
            worksheet_html = ''
            worksheetcalculation = {}
            # woorksheetCalculation['dependency'] = {}
            # worksheetcalculationj = 0
            # list of Dashboards
            dashboard_list = []
            # Get Dashboard HTML
            d3datadashboards = []
            d3_dataoutput = ''
            page3dashboards = ''
            # for Dashboards and worksheets
            twb_dashboards = list(twbroot.find('windows'))
            i = 1
            parsedworksheet_json = {}
            for child in twb_dashboards:
                woorksheet = ''
                # print child.tag, child.attrib
                if child.tag == "window" and child.get('class') == "dashboard":
                    dashboard_list.append(child.get('name'))
                    parsed_json['Dashboard-' + str(i)] = child.attrib
                    # print child.items()
                    k = 1
                    worksheet = '<ul style="list-style-type:none" class="worksheet worksheet-ul">'
                    worksheet_check = ''
                    for step_child in child:
                        # print step_child.tag, step_child.attrib
                        if step_child.tag == "viewpoints" or step_child.tag == "zones":
                            for node in step_child:
                                # print node.tag, node.attrib
                                if node.tag == "viewpoint" or (node.tag == "zone" and len(
                                        node.attrib) == 1):
                                    # print(len(node.attrib))
                                    # (parsed_json['datasources' + str(i)]).update(
                                    # step_child.attrib)
                                    if node.get('name') != '':
                                        worksheet = worksheet + '<li>' + node.get('name') + '</li>'
                                        worksheet_check = worksheet_check + node.get('name')
                                        parsedworksheet_json['Worksheet-' + str(k)] = node.get(
                                            'name')
                                        k = k + 1
                                        d3datadashboards.append({"name": node.get('name'),
                                                                 "icon" : urlscheme + "://" \
                                                                 + request.get_host() \
                                                                 + '/static/img/worksheet.png',
                                                                 "parent": child.get("name")})
                                        # d3_data_datasources.append({"name": item["name"],
                                        #                           "size": size})
                                        # d3_dataworksheet['name'] = node.get('name')
                                        # d3_data['parent'] = parsed_json[
                                        #     'Dashboard-' + str(i)]['name']

                    if worksheet_check != '':
                        parsed_json['Dashboard-' + str(i)]['Worksheets'] = worksheet + '</ul>'

                    # d3datadashboards.append({"name": node.get('name'),
                    #                          "parent": child.get("name")})
                    # print(request.get_host() + '/static/img/dashboard.png')

                    d3_data['children'] = d3_data['children'] \
                        + '{"name": "' + child.get("name") + '", "icon": "' \
                        + urlscheme + "://" +request.get_host() \
                        + '/static/img/dashboard.png", "parent": "' + \
                        parsed_json['workbookInformation'][
                            'Name'] + '", "children": ' + json.dumps(d3datadashboards) + '},'

                    d3datadashboards = []
                    worksheet = ''
                    # parsed_json['Dashboard-' + str(i)]['worksheet_list'] = parsedworksheet_json
                    # Get Size of a dashboard
                    # print(twbroot.findall("./dashboards/dashboard[
                    #     @name='" + child.get('name') + "']/size"))
                    dashboard_size = twbroot.findall(
                        './dashboards/dashboard[@name="' + child.get('name') + '"]/size')
                    # print(dashboard_size[0])
                    for size in dashboard_size:
                        # print size.attrib
                        if size.get('sizing-mode') == 'automatic':
                            parsed_json['Dashboard-' + str(i)]['Size'] = 'Automatic'
                        if size.get('minwidth') and size.get('minheight') != None:
                            parsed_json['Dashboard-' + str(i)][
                                'MinSize'] = 'Min. Width * Height = ' \
                             + size.get('minwidth') + 'px * ' + size.get('minheight') + 'px '
                        if size.get('maxheight') and size.get('maxwidth') != None:
                            parsed_json['Dashboard-' + str(i)][
                                'MaxSize'] = 'Max. Width * Height = ' + size.get(
                                    'maxwidth') + 'px * ' + size.get('maxheight') + 'px'

                    # Get Datasource of dahsboard
                    dashboarddatasource = '<table class="dashboarddatasource page1-table' \
                        'crumbs_table table tbl-width">' \
                        '<tbody><tr class="dashboarddatasource-tr">'
                    print twbroot.findall('./dashboards/dashboard[@name="' + child.get(
                        'name') + '"]/datasources/datasource')
                    dashboard_source = twbroot.findall('./dashboards/dashboard[' \
                        '@name="' + child.get('name') + '"]/datasources/datasource')
                    # print(dashboard_source)
                    dashboard_datasource_name = ''
                    for source in dashboard_source:
                        # print source.attrib
                        dashboard_datasource_name = source.get('caption')
                        if source.get('caption') is None:
                            dashboard_datasource_name = source.get('name')
                        dashboarddatasource = dashboarddatasource + '<td>' \
                            + dashboard_datasource_name + '</td>'
                    if dashboard_datasource_name != '':
                        parsed_json['Dashboard-' + str(i)][
                            'Datasources'] = dashboarddatasource + '</tr></tbody></table>'

                    parsedworksheet_json = {}

                    print parsed_json['Dashboard-' + str(i)]

                    # Remove unwanted elements
                    if 'class' in parsed_json['Dashboard-' + str(i)]:
                        del parsed_json['Dashboard-' + str(i)]['class']
                    if 'maximized' in parsed_json['Dashboard-' + str(i)]:
                        del parsed_json['Dashboard-' + str(i)]['maximized']
                    if 'auto-hidden' in parsed_json['Dashboard-' + str(i)]:
                        del parsed_json['Dashboard-' + str(i)]['auto-hidden']
                    if 'test' in parsed_json['Dashboard-' + str(i)]:
                        del parsed_json['Dashboard-' + str(i)]['test']

                    d3_data["name"] = parsed_json['workbookInformation']['Name']
                    d3_data["parent"] = "null"
                    # d3_data['children'] = d3_data['children']
                    # print(d3_data['children'])
                    d3_dataoutput = '{"name" : "' + parsed_json['workbookInformation'][
                        'Name']  + '",  "icon": "' + urlscheme + "://" + request.get_host()+ \
                        '/static/img/favicon.png", "parent" : "null", "children": [' + d3_data[
                            'children'][:-1] + ']}'
                    # print(d3_dataoutput)
                    # {'name': 'PoC1', 'parent': 'null', 'children':
                    # Dashboard HTML
                    dashboard_json = json.dumps(parsed_json['Dashboard-' + str(i)])
                    dashboard_json = dict((k.title(), v) for k, v in parsed_json[
                        'Dashboard-' + str(i)].iteritems())
                    #replaceName = {'MinSize':'Minsize',
                    #               'name':'Name',
                    #               'MaxSize':'Maxsize',
                    #               'Worksheets':'Worksheets',
                    #               'Datasources':'Datasources'}
                    #dashboard_json = {replaceName[k]: v for k, v in dashboard_json.items()}
                    #dashboard_json = dict((k.title(), v) for k,v in parsed_json[
                    #   'Dashboard-' + str(i)].iteritems())
                    #dashboard_json.update({k.upper(): v for k, v in dashboard_json.iteritems()})
                    # sort_order = ['Name', 'Minsize','Maxsize', 'Worksheets','Datasources']
                    # data-dictionary = sorted(
                    #   datasource_columns.iteritems(), key=lambda (k, v): sort_order.index())
                    # Dashboard_ordered = {}
                    # Dashboard_ordered =sorted(
                    #   dashboard_json.iteritems(), key=lambda (k, v): sort_order.index(k))
                    # print Dashboard_ordered
                    #datadash = {}
                    #dash = for k, v in sorted(dashboard_json.iteritems(), key=lambda (
                    #   k, v): sort_order.index(k)):
                    # Columns.add_column(key, sorted(val))

                    dashboard_html = json2html.convert(json=dashboard_json,
                                                       table_attributes="id=\"dashboard-table\" class=\"page1-table crumbs_table table tbl-width Dashboard-" + str(i) + '"', encode=False, escape=False)
                    dashboard_thumbnail = list(twbroot.findall('./thumbnails/thumbnail[' \
                        '@name="' + child.get('name') + '"]'))

                    dashboard_thumbnail_html = ''
                    if dashboard_thumbnail:
                        for thumbnail in dashboard_thumbnail:
                            dashboard_thumbnail_html = '<div id="' \
                                + child.get('name') + '" class="dashboard dashboard-img">' \
                                '<span> <img class="span-img" src="data:image/png;base64,' \
                                + thumbnail.text + '"/> </span></div>'
                                # print(thumbnails_html)
                    else:
                        thumbnails_page = "No Thumbnails Found"

                    page3dashboards = page3dashboards + '<div class="dashboard Dashboard-' \
                        + str(i) + ' watermarkimg">' \
                        '<div class="dashboard-heading"><span><h2 >' \
                        + child.get('name') + ' </h2> </span></div>'\
                        + dashboard_thumbnail_html + dashboard_html + '</div>'
                    i = i + 1
                # Process Worksheet info.
                elif child.tag == "window" and (child.get(
                        'class') == "worksheet" or child.get('class') == "hidden-worksheet"):
                    worksheet_list.append(child.get('name'))
                    # Worksheet Datasource info
                    # print('./worksheets/worksheet[
                    #     @name="' + re.escape(child.get('name')) + '"]' \
                    #     '/table/view/datasources/datasource')
                    worksheet_datasources = list(twbroot.findall(
                        './worksheets/worksheet[@name="' + child.get(
                            'name') + '"]/table/view/datasources/datasource'))
                    worksheet_datasources_html = ''
                    for worksheet_datasource in worksheet_datasources:
                        # print(worksheet_datasource)
                        worksheet_datasource_name = str(worksheet_datasource.get('caption'))
                        if str(worksheet_datasource.get('caption')) == 'None':
                            datasource_name = str(worksheet_datasource.get('name'))
                            if datasource_name == 'Parameters':
                                worksheet_datasource_name = ""
                            else:
                                worksheet_datasource_name = str(worksheet_datasource.get('name'))
                        worksheet_datasources_html = worksheet_datasources_html + '<td >' \
                            + worksheet_datasource_name + '</td>'
                        # worksheet_datasources_html = worksheet_datasources_html + '<td >' \
                        #    + worksheet_datasource.get('caption') + '</td>' + \
                        #    '<td>' +  worksheet_datasource.get('name') + '</td>'

                    # Worksheet Column info
                    worksheetcolumns = list(twbroot.findall(
                        './worksheets/worksheet[@name="' +  child.get(
                            'name') + '"]/table/view/datasource-dependencies/column'))
                    worksheetcolumns_html = ''
                    worksheetcolumns_html1 = ''
                    worksheetcolumns_html2 = ''
                    for worksheetcolumn in worksheetcolumns:
                        # print(worksheetcolumn)
                        if worksheetcolumn.get('role') == "dimension":
                            bgclass = 'blueBg'
                        elif  worksheetcolumn.get('role') == "measure":
                            bgclass = 'greenBg'
                        else:
                            bgclass = ''

                        column_name = worksheetcolumn.get('name')
                        if worksheetcolumn.get('caption') != None:
                            column_name = worksheetcolumn.get('caption')

                        calculation = worksheetcolumn.getchildren()
                        if calculation and calculation[0].tag == "calculation":
                            if worksheetcalculation.get(str(column_name.encode('utf8'))) is None:
                                worksheetcalculation[str(column_name.encode('utf8'))] = {}
                            # print(worksheetcalculation[str(column_name)])
                            worksheetcalculation[str(column_name.encode('utf8'))] = str(
                                worksheetcalculation[str(column_name.encode(
                                    'utf8'))]) + '<li>'+ child.get('name') + '</li>'
                            # worksheetcalculationj
                            # worksheetcalculation[worksheetcalculationj].worksheet.append(
                            #     child.get('name'))

                        if bgclass == 'blueBg':
                            worksheetcolumns_html1 = worksheetcolumns_html1 + \
                                '<li class="' + bgclass + \
                                '" >' + column_name.replace("'", r"\'") + '</li>'
                        else:
                            worksheetcolumns_html2 = worksheetcolumns_html2 + \
                                '<li class="' + bgclass + \
                                '" >' + column_name.replace("'", r"\'") + '</li>'
                        worksheetcolumns_html = worksheetcolumns_html1 + worksheetcolumns_html2
                    #Build Worksheet HTML

                    woorksheet = '<div class="worksheet watermarkimg">' \
                        '<div class="worksheet-heading"> <span> <h2> ' + child.get('name') + \
                        '</h2></span></div><table class="page1-table crumbs_table"' \
                        'table tbl-width">' \
                        '<tr class="worksheet-datasource">' \
                        '<th> Datasources </th> <td>' \
                        '<table class="dashboard-tbl page1-table crumbs_table table tbl-width">' \
                        '<tr class="worksheet-tr">' + worksheet_datasources_html + \
                        '</tr> </table></td></tr>' \
                        + '<tr class="worksheet-column"><th>Columns</th>' \
                        '<td><ul class="worksheet-column-ul">' + worksheetcolumns_html + \
                        '</ul></td></tr></table></div>'
                    #worksheet HTML Ends

                worksheet_html = worksheet_html + woorksheet

            print 'worksheetcalculation = ', worksheetcalculation
            print 'DashboardList = '
            print list(dashboard_list)
            print 'worksheet_list = '
            print list(worksheet_list)

            xml_dashboardlist_json = json.dumps(parsed_json)
            xml_dashboardlist = json2html.convert(json=xml_dashboardlist_json,
                                                  table_attributes="id=\"dashboard-table\"" \
                                                  "class=\"dashboard\"")

            # Thumbnails Processing Start
            thumbnails_page = ''
            if twbroot.find('thumbnails') != None:
                twb_thumbnails = list(twbroot.find('thumbnails'))
                if twb_thumbnails:
                    for child in twb_thumbnails:
                        # print child.tag, child.attrib
                        if child.get('name') in dashboard_list: # Only dashboards thumbanils
                            thumbnails_html = '<div id="' + child.get('name') + \
                                '" class="snapshot-img"> <h3>' + child.get('name') \
                                + ' </h3><span><img class="span-img"' \
                                'src="data:image/png;base64,' + child.text + '"/>' \
                                '</span></div>'

                            thumbnails_page = thumbnails_page + thumbnails_html
                            # print(thumbnails_html)
                else:
                    thumbnails_page = "No Thumbnails Found"

            # Thumbnails Processing END

            # Page Formatting Starts

            worksheet_calculation_html = json.dumps(worksheetcalculation)
            worksheet_calculation_html = json2html.convert(json=worksheet_calculation_html,
                                                           table_attributes="id=\"woorksheetCalculation-table\" align=\"center\" class=\"page1-table tbl-collapse crumbs_table tbl-head table tbl-width\"",
                                                           encode=False,
                                                           escape=False)
            worksheet_calculation_html = worksheet_calculation_html.replace('{}', "")
            worksheet_calculation_html = worksheet_calculation_html.replace(
                '<table id="woorksheetCalculation-table" class="woorksheetCalculation">',
                '<table id="woorksheetCalculation-table" class="woorksheetCalculation"' \
                'style="margin: 0 auto;"><thead><th>Calculation Name</th>' \
                '<th>Worksheet Dependency</th></thead>')


            body_content = js_script+ page1 + '</div> '+ page2 + \
                '<p class="pb_before"><br /></p> <div id="snapshots" class="snapshots">' \
                '<div id="thumbnails" class="thumbnails"><h1>Thumbnails</h1> </div>' \
                + thumbnails_page + '</div> </div> <p class="pb_before"></p>' \
                '<div class="page2d3js"> <div id="container">' \
                '<div class="drop-shadow lifted"><p>Workbook</p></div>' \
                '<div class="drop-shadow lifted">' \
                '<p>Dashboard</p></div><div class="drop-shadow lifted">' \
                '<p>Worksheet</p></div><div class="page2d3"></div></div></div>'\
                + '<p class="pb_before"></p><div class="page3 "> <div> ' \
                '<h1 class="page3-h1 div-color">Dashboard Details</h1></div>' \
                + page3dashboards + '<p class="pb_before"></p> <div>' \
                '<h1 class="div-color">Worksheet Details</h1></div>' \
                + worksheet_html + '</div>' \
                + '<p class="pb_before"></p><div class="page4 watermarkimg">' \
                '<div><h1 class="page4-h1 div-color">Datasource Details</h1></div>' \
                '<div class="used-columns">' + page4datasourcedetails + '</div></div>' \
                + '<p class="pb_before"></p><div class="page5 watermarkimg">' \
                '<div><h1 class="page5-h1 div-color">Unused Column Details</h1></div>' \
                + "<h3>Get Full version to unlock </h3>" \
                + '</div><p class="pb_before"></p><div class="page6 watermarkimg">' \
                '<div><h1 class="page6-h1 div-color">Worksheet Calculation Dependency</h1>' \
                '</div><div class="woorksheetCalculation">' \
                + "<h3>Get Full version to unlock</h3>" + '</div></div>'

                # '<div><table class="page1-table crumbs_table tbl-head table tbl-width">' \
                # '<tbody><tr><th>Name</th><th>Datatype</th><th>Role</th><th>Type</th></tr>' \
                # + unused_html + '</tbody></table></div>' \
                # + worksheet_calculation_html + '</div></div>'

                          # + xml_dashboardlist
            # Page Formatting Ends

            # footer = os.getcwd() + '/CookieCrumbles/static/partials/pdffooter.html'
            # footer = footer.replace('/', "\\")
            footer = urlscheme + "://" + request.get_host() + '/static/partials/pdffooter.html'
            print footer
            # "http://" + request.get_host()+ media_uploads +
            # "http://crumbles.app:82/static/partials/pdffooter.html",

            # header = os.getcwd() + '/CookieCrumbles/static/partials/pdfheader.html'
            # header = header.replace('/', "\\")
            header = urlscheme + "://" + request.get_host() + '/static/partials/pdfheader.html'
            print header
            # "http://crumbles.app:82/static/partials/pdfheader.html",

            html_text = markdown(body_content, output_format='html4')
            options = {
                'page-size': 'Letter',
                'margin-top': '0.5in',
                'margin-right': '0.25in',
                'margin-bottom': '0.5in',
                'margin-left': '0.25in',
                'encoding': "UTF-8",
                'header-html': header,
                'footer-html':  footer,
                'footer-right': '[page] of [topage]',
            }

            # pass the CSS for the Formating
            # css = os.getcwd() + '\CookieCrumbles\static\parser\pdf.css'
            # css = css.replace('/', "\\")
            # css = r'C:\MAMP\htdocs\crumbles\crumbles\CookieCrumbles\static\parser\style.css'
            # css = urlscheme + "://" + request.get_host() + '/static/parser/pdf.css'
            css = os.getcwd()  + '/static/parser/pdf.css'
            css = css.replace('\\', "/")
            print css

            # css = str(".datasource, .datasource th, .datasource td,
            #    .dashboard, .dashboard th, .dashboard td{ border: 1px solid;}")
            output_filename = os.path.splitext(tableau_file)[0]
            output_filename = str(output_filename) + '.pdf'

            #HTML File Output
            crumbstime = time.time()
            crumbstimestap = datetime.datetime.fromtimestamp(
                crumbstime).strftime('%Y-%m-%d %H:%M:%S')
            print 'Started Html file generated' + crumbstimestap
            html_file = open(os.getcwd() + media_uploads + filename + '/output/' + str(
                os.path.splitext(tableau_file)[0]) + '.html', "w")
            html_file.write('<style>')
            html_file.write(open(css, "r").read())
            # html_file.write(urllib.urlopen(css).read())
            html_file.write('</style>')
            # TODO this might fail if not internet as css have google fonts
            html_file.write('<script  type="text/javascript"> var flare = ' \
                + d3_dataoutput +';</script>')

            js_path = os.getcwd() + '/static/parser/jquery-1.10.2.js'
            js_path = js_path.replace('\\', "/")
            html_file.write('<script  type="text/javascript">')
            html_file.write(open(js_path, "r").read())
            # html_file.write(urllib.urlopen(js_path).read())
            html_file.write('</script>')

            html_file.write(html_text.encode("UTF-8"))
            js_path = os.getcwd() + '/static/parser/d3.v3.min.js'
            js_path = js_path.replace('\\', "/")
            html_file.write('<script  type="text/javascript">')
            html_file.write(open(js_path, "r").read())
            # html_file.write(urllib.urlopen(js_path).read())
            html_file.write('</script>')

            js_path = os.getcwd() + '/static/parser/d3_tree.js'
            js_path = js_path.replace('\\', "/")
            html_file.write('<script  type="text/javascript">')
            html_file.write(open(js_path, "r").read())
            # html_file.write(urllib.urlopen(js_path).read())
            html_file.write('</script>')

            # html_file.write(open(footer, "r").read())
            html_file.close()

            html_file_path = urlparse(os.getcwd() + media_uploads + filename + \
                '/output/' + str(os.path.splitext(tableau_file)[0]) + '.html')
            html_file_path = html_file_path.geturl()
            html_file_path = html_file_path.replace('\\', "/")
            print html_file_path
            crumbstime = time.time()
            crumbstimestap = datetime.datetime.fromtimestamp(
                crumbstime).strftime('%Y-%m-%d %H:%M:%S')
            print 'Complated Html file generated' + crumbstimestap
            crumbstime = time.time()
            crumbstimestap = datetime.datetime.fromtimestamp(
                crumbstime).strftime('%Y-%m-%d %H:%M:%S')
            print 'Started PDF file generated' + crumbstimestap
            #PDF file Output
            pdfkit.from_file(html_file_path, os.getcwd() + media_uploads \
                + filename + '/output/' + output_filename, options=options)

            pdf_file_path = urlparse(media_uploads + filename + '/output/' + output_filename)
            pdf_file_path = pdf_file_path.geturl()
            print pdf_file_path
            crumbstime = time.time()
            crumbstimestap = datetime.datetime.fromtimestamp(
                crumbstime).strftime('%Y-%m-%d %H:%M:%S')
            print 'Complated PDF file generated' + crumbstimestap

            # Get the summary information from the file
            extract_summary = ''
            # return render(request, 'parser/index.html', {
            return render(request, 'parser/crumbles.html', {
                'uploaded_file_url': uploaded_file_url,
                'full_path': url,
                'tableauFile': tableau_file,
                # 'extract_file_list': extract_file_list,
                # 'xml_datasource_list': json.loads(xml_datasource_list),
                'xmlDatasourceList': xml_datasource_list,
                'xmlDashboardListJSON': xml_dashboardlist_json,
                'xmlDashboardList': xml_dashboardlist,
                'PDFFilePath': pdf_file_path,
                'extractSummary': extract_summary,
                'Columns': columns,
                'bodyContent': body_content,
                'page3Dashboards': page3dashboards,
                'd3Data': d3_dataoutput,
            })
        return render(request, 'parser/index.html')
    except (OSError, IOError, NameError, KeyError, UnicodeDecodeError) as err:
        print err
        print traceback.format_exc()
        # print traceback.print_exc()
        # print("Unexpected error:", sys.exc_info()[0])
        # raise Http404("File does not exist")
        # pass
        # return render(request, 'helpers/error.html')# return with disabled error for Prod.
        # return render(request, 'helpers/error.html', {'error_message': err})
        try:
            body_email = '<br /><br /><b> File Name :  </b>' + str(filename) +\
                         '<br /><br /><b> URL with path:  </b>' \
                         '<a href="' + urlscheme + "://" + request.get_host() \
                         + uploaded_file_url + '" target="_blank" " >' + url +'</a>'+ \
                         '<br /><br /><b> User Email: </b>'+ str(email_id) + \
                         '<br /><br /><b>Error : </b>' + str(err) + \
                         '<br /><br /> <b>Traceback : </b>' + str(traceback.format_exc())
            email = EmailMessage(
                'Error Reported from crumbs',
                body_email,
                from_email,
                to_email,
            )
            email.content_subtype = "html"
            email.send()
            print 'Mail Sent'
        except:
            print "Unexpected error:", sys.exc_info()
            print traceback.print_exc()
            pass
        return render(request,
                      'helpers/error.html',
                      {'error_message': list(sys.exc_info()),
                       'traceback': traceback.print_exc(),
                       'url' : url,
                       'email_id': email_id})
    except:
        print "Unexpected error:", sys.exc_info()[0]
        print traceback.format_exc()
        # return render(request, 'helpers/error.html')# return with disabled error for Prod.
        try:
            body_email = '<br /><br /><b> File Name :  </b>' + str(filename) +\
                         '<br /><br /><b> URL with path:  </b>' \
                         '<a href="' + urlscheme + "://" + request.get_host() \
                         + uploaded_file_url + '" target="_blank" " >'+ url +'</a>'+\
                         '<br /><br /><b> User Email: </b>'+ str(email_id) + \
                         '<br /><br /><b>Error : </b>' + str(sys.exc_info()[0]) + \
                         '<br /><br /> <b>Traceback : </b>' + str(traceback.format_exc())
            email = EmailMessage(
                'Error Reported from crumbs',
                body_email,
                from_email,
                to_email,
            )
            email.content_subtype = "html"
            email.send()
            print 'Mail Sent'
        except:
            print "Unexpected error:", sys.exc_info()[0]
            print traceback.print_exc()
            pass
        return render(request, 'helpers/error.html',
                      {'error_message': list(sys.exc_info()),
                       'traceback': traceback.print_exc(),
                       'url' : url,
                       'email_id': email_id})
    # return HttpResponse(template.render(request))
    return render(request, 'helpers/error.html')
