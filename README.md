Crumbles - Your Cookie Data

Install Notes

Install Python 2.7

python -m pip install -U pip

**PIP might have issue in root user in ec2, recommended to try on ec-user or try with sudo su - **

pip install django==1.8.*

pip install pdfkit json2html markdown prettytable wkhtmltopdf

SQLLite DB browser

Install http://wkhtmltopdf.org/downloads.html and Copy the wkhtmltopdf to python working dir.
To install wkhtml in EC2

Download the files from the above link and 

yum -y install xz

unxz filename.tar.xz

tar -xf filename.tar



To install server

yum install python-devel make automake nginx gcc gcc-c++ python-setuptools

sudo pip install uwsgi


** Gulp **

To install gulp

node -v

npm -v

npm install

sudo npm install gulp -g

gulp -v

run gulp by command by 'gulp'

Ubuntu Font

http://font.ubuntu.com/download/ubuntu-font-family-0.83.zip
cd /mnt/data
wget http://font.ubuntu.com/download/ubuntu-font-family-0.83.zip
cp -r /mnt/data/ubuntu /usr/share/fonts/
