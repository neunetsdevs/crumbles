"""
Django settings for crumbles project.

Generated by 'django-admin startproject' using Django 1.10.4.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.10/ref/settings/
"""

import os
from django.conf import global_settings


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 's+elt#%5%w!gj9r9um)@44ylec$3yrfttblp@@9*_!th+vv=p2'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
# DEBUG = False
#DEBUG404 = False
DEBUG404 = True

ALLOWED_HOSTS = [u'crumbs.app', u'crumbs.cookieanalytix.com', u'127.0.0.1', u'handbook.cookieanalytix.com', u'10.10.1.7', u'crumbs-554122724.us-east-1.elb.amazonaws.com', u'10.10.1.117', u'ec2-34-200-110-211.compute-1.amazonaws.com', u'handbook-uat.cookieanalytix.com']

#set HTTPS header
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_gulp',
    'gulp_rev',
    'CookieCrumbles',
]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'crumbles.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, "templates"),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.template.context_processors.media',
            ],
        },
    },
]

WSGI_APPLICATION = 'crumbles.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

#x Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Asia/Calcutta'
USE_I18N = True
USE_L10N = True
USE_TZ = True
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = '/static/'

# STATICFILES_DIRS = (
#   os.path.join(BASE_DIR, 'static'),
# )

# STATICFILES_DIRS = (
#     os.path.join(os.path.dirname(BASE_DIR), 'static'),
# )

STATIC_ROOT = 'static' #os.path.join(os.path.dirname(BASE_DIR), 'static')
# STATIC_ROOT = os.path.join(BASE_DIR, 'static')
# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

#Gulp Rev-manifest file path

DJANGO_GULP_REV_PATH = STATIC_ROOT + '/build/rev-manifest.json'


#TEMPLATES DIR
# TEMPLATE_DIR = ''
# BASE_URL = 'http://crumbles.app'

EMAIL_USE_TLS = True
EMAIL_HOST = 'email-smtp.us-east-1.amazonaws.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'AKIAJFX6VX22P4MULVLA'
EMAIL_HOST_PASSWORD = 'AvW0A/gYJWWjRk2icPHLYsZfX0qKEToUVi+WR4mDaGNH'

# EMAIL_HOST = 'smtp.sendgrid.net'
# EMAIL_HOST_USER = 'hello_neunets'
# EMAIL_HOST_PASSWORD = 'H3l!0@321'
# EMAIL_PORT = 587
# EMAIL_USE_TLS = True

#logging

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'WARNING',
            'class': 'logging.FileHandler',
            'filename': 'debug.log',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'WARNING',
            'propagate': True,
        },
    },
}
