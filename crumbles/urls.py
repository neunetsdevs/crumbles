"""crumbles URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from CookieCrumbles import views
from django.conf import settings
from django.contrib.auth.views import login, logout
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import os
from django.conf.urls import (
handler400, handler403, handler404, handler500
)
import django.views.defaults

# handler404 = '404.html'

handler404 = views.handler404

# handler404 = views.error404
# handler500 = 'views.page_error_found_custom'
#
# handler400 = 'my_app.views.bad_request'
# handler403 = 'my_app.views.permission_denied'
# handler404 = 'my_app.views.page_not_found'
# handler500 = 'my_app.views.server_error'

urlpatterns = [
    # the regex ^$ matches empty
    url(r'^$', views.parser, name='parser'),
    url(r'^CookieCrumbles/', include('CookieCrumbles.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/login', views.login_page, name='login'),
    #url(r'^accounts/login_p/', auth_views.login, name='login_p'),
    url(r'^logout/', views.logout_page, name='logout_page'),
    # url(r'^datasourceexport/', views.datasourceexport, name='datasourceexport'),
    url(r'^parser/', views.parser, name='parser'),
    url(r'^eula/', views.eula, name='eula'),
    # url(r'^', include('filer.server.urls')),
    # url(r'^404/$', django.views.defaults.page_not_found, ),
    # url(r'^404/$', views.error404, name='error404'),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.STATIC_ROOT}),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
]

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

#
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    # urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    # STATICFILES_DIRS = (
    #     os.path.join((BASE_DIR), 'static'),
    # )
#
if settings.DEBUG404:
    print(settings.MEDIA_ROOT)
    settings.MEDIA_ROOT = settings.MEDIA_ROOT.replace('\\', "/")
    print(settings.MEDIA_ROOT)
    print(settings.STATIC_ROOT)
    settings.STATIC_ROOT = settings.STATIC_ROOT.replace('\\', "/")
    print(settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    STATICFILES_DIRS = (
        settings.STATIC_ROOT,
    )
    print(STATICFILES_DIRS)
    # STATICFILES_DIRS = STATICFILES_DIRS.replace('\\', "/")
    # print(STATICFILES_DIRS)

# if settings.DEBUG404:
#     urlpatterns += urlpatterns('',
#         (r'^static/(?P<path>.*)$', 'django.views.static.serve',
#          {'document_root': os.path.join(os.path.dirname(__file__), 'static')} ),
#     )
# if settings.DEBUG:
#     MEDIA_URL = '/media/'
#     MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
#     STATIC_URL = '/static/'
#     STATIC_ROOT = os.path.join((BASE_DIR), 'static')
#     STATICFILES_DIRS = (
#         os.path.join((BASE_DIR), 'static'),
#     )
